import React from 'react';

import Header from '../../components/header/Header';
import Footer from '../../components/footer/Footer';
import HomeAbout from '../../components/home/home-about/HomeAbout';
import HomeAction from '../../components/home/home-action/HomeAction';
import HomeBenefits from '../../components/home/home-benefits/HomeBenefits';
import HomeHero from '../../components/home/home-hero/HomeHero';
import HomeOurWork from '../../components/home/home-our-work/HomeOurWork';
import HomePartners from '../../components/home/home-partners/HomePartners';
// import HomeRequest from '../../components/home/home-request/HomeRequest';
import HomeContacts from '../../components/home/home-contacts/HomeContacts';
import StickyName from '../../components/sticky-name/StickyName';

import videoBg from '../../asset/file/main.mp4';

import './Home.scss';

const Home = () => {
    return (
        <>
            <div className='hero-video-bg-wrapper'>
                <Header />
                <HomeHero />
                <HomeBenefits />

                <video className='hero-video-bg' autoPlay="autoplay" playsInline muted loop>
                    <source src={videoBg} type="video/mp4" />
                </video>
            </div>
            <HomeAction />
            <HomeOurWork />
            <HomeAbout />
            {/* <HomeRequest /> */}
            <HomePartners />
            <HomeContacts />
            <Footer />
            <StickyName />
        </>
    )
};

export default Home;