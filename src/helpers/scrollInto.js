export const scrollInto = (elem) => {
    elem.scrollIntoView({block: "center", behavior: "smooth"});
}