import React, { useState, useEffect, useCallback, useRef } from 'react';

import './StickyName.scss';

const defaultStickyValues = {
    number: '01',
    title: 'главная',
}
const StickyName = () => {
    const [stickyValues, updateStickyValues] = useState(defaultStickyValues);
    const stickyNameRef = useRef();

    const handleScroll = useCallback((event) => {
        // when scroll to top
        if (window.scrollY === 0) {
            updateStickyValues(defaultStickyValues);
            return;
        }
        const stikyElem = document.querySelectorAll('.js-stiky-name');
        const windowHeight = window.innerHeight;

        let newStickyValues;
        for (let i = 0; i < stikyElem.length; i++) {
            const elemPositionY = stikyElem[i].getBoundingClientRect().y;

            if (elemPositionY - windowHeight + (windowHeight / 2) < 0) {
                const number = stikyElem[i].dataset.stikyNumber;
                const title = stikyElem[i].dataset.stikyTitle;

                newStickyValues = {
                    number,
                    title,
                }
            }     
        }

        if (newStickyValues) {
            updateStickyValues(newStickyValues); 
        }
    }, []);

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);

        return () => window.removeEventListener('scroll', handleScroll);
    }, [handleScroll]);

    useEffect(() => {
        if (stickyNameRef) {
            stickyNameRef.current.animate(
            [
                { opacity: '0' },
                { opacity: '1' }
            ],
            {
                duration: 1000,
                iterations: 1,
                easing: 'ease-out',
            }
            );
        }
    }, [stickyValues.number, stickyNameRef]);
            
    return (
        <div ref={stickyNameRef} className='sticky-name'>
            <span className='sticky-name__number'>{ stickyValues.number }</span>
            <span className='js-sticky-name-title sticky-name__title'>{ stickyValues.title }</span>
        </div>
    )
};

export default StickyName;