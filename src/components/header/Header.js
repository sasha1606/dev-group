import React from 'react';

import logoImg from '../../asset/img/logo.svg';
import logoMobImg from '../../asset/img/logo-mob.svg';
import squaresImg from '../../asset/img/icon/squares.svg';
import phoneImg from '../../asset/img/icon/phone.svg';

import './Header.scss';


const Header = () => {
            
    return (
        <div className='container'>
            <div className='header'>
                <img className='header__logo display-only-desktop' src={logoImg} alt='' />
                <img className='header__logo display-only-mob' src={logoMobImg} alt='' />
                <img className='header__squares' src={squaresImg} alt='' />
                <a href='tel:0950033335' className='not-link header__tel'>
                    <span className='display-only-desktop'>+380950033335</span>
                    <img className='display-only-mob' src={phoneImg} alt='' />
                </a>
            </div>
        </div>
    )
};

export default Header;