import React from 'react';

import logoImg from '../../asset/img/logo-icon.svg';

import './Footer.scss';

const Footer = () => {
            
    return (
        <div className='footer-wrapper'>
            <div className='container'>
                <div className='footer'>
                    <img className='footer__logo' src={logoImg} alt='' />
                </div>
            </div>
        </div>
    )
};

export default Footer;