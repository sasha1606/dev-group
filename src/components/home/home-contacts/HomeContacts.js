import React, { useState } from 'react';

import mailImg from '../../../asset/img/icon/mail.svg';
import markerImg from '../../../asset/img/icon/marker.svg';

import './HomeContacts.scss';

const HomeContacts = () => {
    const [ nameValue, updateNameValue ] = useState('');
    const [ telValue, updateTelValue ] = useState('');
    const [ isDisabledForm, updateIsDisabledForm ] = useState(false);
    const [errors, updateErrors] = useState({
        name: null,
        tel: null
    })

    const onRequest = async () => {
        if (!validation()) {
            return false;
        }

        updateIsDisabledForm(true);
    
        // requirements from the product manager
        setTimeout(() => updateIsDisabledForm(false), 3000);

        try {
            await fetch('https://back.weway.io/develop-group', {
                method: 'POST',
                body: JSON.stringify(
                    { 
                        name: nameValue,
                        phone: telValue
                     }
                ),
                // headers: {
                //   'Content-Type': 'application/json'
                // }
              });
            //   const json = await response.json();
              nameValue('');
              telValue('');
        } catch (error) {
            console.error('Ошибка:', error);
        }
    };

    const onChangeName = (value) => {
        updateNameValue(value);
        updateErrors({...errors, name: null })
    }

    const onChangeTel = (value) => {
        updateTelValue(value);
        updateErrors({...errors, tel: null })
    }

    const validation = () => {
        let isFromValid = true;
        let errors = {
            name: null,
            tel: null
        }

        if (!validateName(nameValue)) {
            errors.name = true;
            isFromValid = false;
        }

        if (!validateTel(telValue)) {
            errors.tel = true;
            isFromValid = false;
        }

        updateErrors(errors)
        return isFromValid;
    }

    function validateName(name) {
        // const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        // return re.test(String(email).toLowerCase());
        return name.length > 3;
    }

    function validateTel(tel) {
        const re = /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/;
        return re.test(String(tel).toLowerCase());
    }

    return (
        <div id='home-contacts' className='home-contacts-wrapper js-stiky-name' data-stiky-number='06'  data-stiky-title='Контакты'>
            <div className='container'>
                <div className='home-contacts'>
                    <h4 className='title home-contacts__title'>
                        Контакты
                    </h4>
                    <div className='home-contacts__inner'>
                        <div className='home-contacts__form-wrap'>
                            <h6 className='home-contacts__sub-title'>
                                У вас остались вопросы?
                            </h6>
                            <form>
                                <div className='home-contacts__from-group'>
                                    <input
                                        disabled={isDisabledForm}
                                        type='text'
                                        className={`${errors.name ? 'home-contacts__control_error' : ''} home-contacts__control`}
                                        placeholder='Имя'
                                        value={nameValue}
                                        onChange={({ target: { value } }) => onChangeName(value)}
                                    />
                                </div>
                                <div className='home-contacts__from-group home-contacts__from-group_right'>
                                    <input
                                        disabled={isDisabledForm}
                                        type='text'
                                        className={`${errors.tel ? 'home-contacts__control_error' : ''} home-contacts__control`}
                                        placeholder='Телефон'
                                        value={telValue}
                                        onChange={({ target: { value } }) => onChangeTel(value)}
                                    />
                                    <button
                                        disabled={isDisabledForm}
                                        type='button'
                                        className='btn home-contacts__send-request'
                                        onClick={onRequest}>
                                            Оставить заявку
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div className='home-contacts__footer'>
                            <div className='home-contacts__info'>
                                <div className='home-contacts__info-icon-wrap'>
                                    <img className='home-contacts__info-icon' src={mailImg} alt='' />
                                </div>
                                <a href="mailto:developGroupMVP@gmail.com" className='home-contacts__info-text home-contacts__info-text_link'>
                                    DevelopGroupMVP@gmail.com
                                </a>
                            </div>
                            <div className='home-contacts__info'>
                                <div className='home-contacts__info-icon-wrap'>
                                    <img className='home-contacts__info-icon home-contacts__info-icon_marker' src={markerImg} alt='' />
                                </div>
                                <span href="mailto:developGroupMVP@gmail.com" className='home-contacts__info-text'>
                                    ул. Базовая 8,
                                    <br />
                                    Самарский район, Днепр, 49000.
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default HomeContacts;