import React from 'react';

import Slider from "react-slick";

import arrowIcon from '../../../asset/img/icon/arrow_sm.svg';

import { ReactComponent as PartnerImg1 } from '../../../asset/img/home-partners/partners_1.svg';
import { ReactComponent as PartnerImg2 } from '../../../asset/img/home-partners/partners_2.svg';
import { ReactComponent as PartnerImg3 } from '../../../asset/img/home-partners/partners_3.svg';

import './HomePartners.scss';

const HomePartners = () => {

    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: <div><img className='custom-slick-arrow-icon' src={arrowIcon} alt='' /></div>,
        prevArrow: <div><img className='custom-slick-arrow-icon' src={arrowIcon} alt='' /></div>
      };
   
    return (
        <div className='home-partners-wrapper js-stiky-name' data-stiky-number='05'  data-stiky-title='Партнеры'>
            <div className='container'>
                <div className='home-partners'>
                    <h4 className='title home-partners__title'>
                        Партнеры
                    </h4>

                    <div className='display-only-mob custom-slick-slider'>
                        <Slider {...settings}>
                            <div className="custom-slick-slider__item">
                                <PartnerImg1 className='custom-slick-slider__img' />
                            </div>
                            <a href='https://poolbod.com.ua' target="_blank" className="custom-slick-slider__item home-partners-grid__item_link">
                                <PartnerImg2 className='custom-slick-slider__img' /> 
                            </a>
                            <div className="custom-slick-slider__item">
                                <PartnerImg3 className='custom-slick-slider__img' />  
                            </div>
                        </Slider>
                    </div>

                    <div className='display-only-desktop'>
                        <div className='home-partners-grid section-name-wrap'>
                            <div className='home-partners-grid__item'>
                                <PartnerImg1 className='home-partners-grid__img' />
                            </div>

                            <a href='https://poolbod.com.ua' target="_blank" className='home-partners-grid__item home-partners-grid__item_link'>
                                <PartnerImg2 className='home-partners-grid__img' />
                            </a>

                            <div className='home-partners-grid__item'>
                                <PartnerImg3 className='home-partners-grid__img' />
                            </div>

                            {/* <div className='section-name'>
                                <span className='section-name__number'> 05 </span>
                                <span className='section-name__title'>Партнеры</span>
                            </div> */}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default HomePartners;