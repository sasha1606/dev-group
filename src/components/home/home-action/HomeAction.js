import React from 'react';

import arrowImg from '../../../asset/img/icon/arrow.svg';
import { scrollInto } from '../../../helpers/scrollInto';

import './HomeAction.scss';

const HomeAction = () => {

    const onRequest = () => {
        const elem = document.getElementById('home-contacts');
        scrollInto(elem);
    }

    return (
        <div className='container'>
            <div className='home-action'>
                <div onClick={onRequest} className='home-action__item'>
                    <div className='home-action__item-inner'>
                        <p className='home-action__item-name'>
                            Pасчет в подарок
                        </p>
                        <span className='home-action__item-icon-wrap'>
                            <img className='home-action__item-icon' src={arrowImg} />
                        </span>
                    </div>
                </div>
                <div onClick={onRequest} className='home-action__item'>
                    <div className='home-action__item-inner'>
                        <p className='home-action__item-name'>
                            Оставить заявку
                        </p>
                        <span className='home-action__item-icon-wrap'>
                            <img className='home-action__item-icon' src={arrowImg} />
                        </span>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default HomeAction;