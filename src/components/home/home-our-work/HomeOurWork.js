import React from 'react';
import {Carousel} from '3d-react-carousal';

import  img from '../../../asset/img/hero/hero.png'

import './HomeOurWork.scss';

const HomeOurWork = () => {

    const slides = [
        <div className='home-our-work-grid__item'>
            <div className='home-our-work-grid__item-img home-our-work-grid__item-img_1'/>
            <div className='home-our-work-grid__item-footer'>
                <p className='home-our-work-grid__item-volume'>
                    135 м<sup>2</sup>
                </p>
                <p className='home-our-work-grid__item-name'>
                    КГ. Нові Плюти
                </p>
                {/* <p className='home-our-work-grid__item-price'>
                    1 000 000
                </p> */}
            </div>
        </div>,
        <div className='home-our-work-grid__item'>
            <div className='home-our-work-grid__item-img home-our-work-grid__item-img_2'/>
            <div className='home-our-work-grid__item-footer'>
                <p className='home-our-work-grid__item-volume'>
                    1000 м<sup>2</sup>
                </p>
                <p className='home-our-work-grid__item-name'>
                    Клубний Дім
                </p>
                {/* <p className='home-our-work-grid__item-price'>
                    1 000 000
                </p> */}
            </div>
        </div>,
        <div className='home-our-work-grid__item'>
            <div className='home-our-work-grid__item-img home-our-work-grid__item-img_3'/>
            <div className='home-our-work-grid__item-footer'>
                <p className='home-our-work-grid__item-volume'>
                    150 м<sup>2</sup>
                </p>
                <p className='home-our-work-grid__item-name'>
                    КГ. Нові Плюти
                </p>
                {/* <p className='home-our-work-grid__item-price'>
                    1 000 000
                </p> */}
            </div>
        </div>,
    ];
   
    return (
        <div className='container js-stiky-name' data-stiky-number='03'  data-stiky-title='наши работы'>
           
            <div className='home-our-work carousel-3d'>
                <h4 className='title home-our-work__title'>
                    Наши работы
                </h4>
                <div className='display-only-mob'>
                    <Carousel slides={slides} interval={1000}/>
                </div>
                <div className='home-our-work-grid section-name-wrap display-only-desktop'>
                    <div className='home-our-work-grid__item home-our-work-grid__item_count'>
                        <div>
                            <p className='home-our-work-grid__count'>
                                3
                            </p>
                            <p className='home-our-work-grid__last'>
                                последних <br/> проекта
                            </p>
                        </div>
                    </div>

                    {
                        slides.map((item, id) => item)
                    }

                    {/* <div className='section-name'>
                        <span className='section-name__number'> 03</span>
                        <span className='section-name__title'>наши работы</span>
                    </div> */}
                </div>
            </div>
        </div>
    )
};

export default HomeOurWork;