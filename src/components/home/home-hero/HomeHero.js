import React, { useRef } from 'react';

import presentation from '../../../asset/file/dev_group_presentation.pdf';
// import videoBg from '../../../asset/file/main.mp4';

import pdfImg from '../../../asset/img/icon/pdf.svg';
import moreImg from '../../../asset/img/icon/more.svg';
import instagramImg from '../../../asset/img/icon/instagram.svg';
import facebookImg from '../../../asset/img/icon/facebook.svg';
import playImg from '../../../asset/img/icon/play.svg';

// import Header from '../../header/Header';

import './HomeHero.scss';
import { scrollInto } from '../../../helpers/scrollInto';


const HomeHero = () => {

    const videoRef = useRef(null);

    const onPlayVideo = () => {
        const videoElem = videoRef.current;
        if (videoElem.requestFullscreen) {
            videoElem.requestFullscreen();
        }
        else if (videoElem.msRequestFullscreen) {
            videoElem.msRequestFullscreen();
        }
        else if (videoElem.mozRequestFullScreen) {
            videoElem.mozRequestFullScreen();
        }
        else if (videoElem.webkitRequestFullScreen) {
            videoElem.webkitRequestFullScreen();
        }
        videoElem.play();
    };

    const onRequest = () => {
        const elem = document.getElementById('home-contacts');
        scrollInto(elem);
    }
            
    return (
        <div className='container js-stiky-name' data-stiky-number='01'  data-stiky-title='главная'>
            <div className='home-hero'>
                <div className='section-name-wrap'>
                    <div className='home-hero__body'>
                        <h1 className='home-hero__title'>
                            Develop Group — гарантия и качество строительства 
                            комерческих и частных проектов 
                        </h1>
                        <div className='home-hero__action'>
                            <button type='button' onClick={onRequest} className='btn home-hero__request'>
                                Оставить заявку 
                            </button>
                            <a href={presentation} target="_blank" className='home-hero__presentation not-link'>
                                <img className='home-hero__presentation-icon' src={pdfImg} alt='' />
                                <span className='home-hero__presentation-text'>
                                    скачать презентацию
                                </span>
                            </a>
                        </div>
                    </div>

                    {/* <div className='section-name'>
                        <span className='section-name__number'> 01</span>
                        <span className='section-name__title'>главная</span>
                    </div> */}
                </div>
                <div className='home-hero__footer'>
                    <div className='home-hero__more'>
                        <span className='home-hero__scroll'>
                            <img className='home-hero__scroll-icon' src={moreImg} alt='' />
                            <span className='home-hero__scroll-text' src={moreImg} alt=''>
                                листайте вниз
                            </span>
                        </span>
                        <div className='home-hero__socials-icons'>
                            <a className='home-hero__socials-icons-item home-hero__socials-icons-item_instagram' target='_blank' href='https://instagram.com/develop_group_'>
                                <img src={instagramImg} alt='' />
                            </a>

                            <a className='home-hero__socials-icons-item home-hero__socials-icons-item_facebook' target='_blank' href='https://facebook.com/DEVEL0PGR0UP'>
                                <img src={facebookImg} alt='' />
                            </a>
                        
                        </div>
                    </div>

                    {/* <div className='home-hero__video-wrapper'>
                        <div className='home-hero__video-info'>
                            <p className='home-hero__video-title'>
                                Новое видео
                            </p>
                            <p className='home-hero__video-content'>
                                Тонкости при выборе 
                                <br />
                                технологии строительства
                            </p>
                        </div>

                        <div onClick={onPlayVideo} className='home-hero__video-img'>
                            <img className='home-hero__video-play-icon' src={playImg} alt='' />
                        </div>

                        
                    </div> */}
                </div>
            </div>
            {/* <video
                width='0'
                height='0'
                ref={videoRef}
                className='hero__video' 
                >
                <source src="http://www.w3schools.com/html/mov_bbb.mp4"  type="video/mp4" />
                    Your browser does not support the video tag.
            </video> */}
        </div>
    )
};

export default HomeHero;