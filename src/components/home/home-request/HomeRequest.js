import React, { useState } from 'react';

import './HomeRequest.scss';

const HomeRequest = () => {

    const [ inputTel, updateInputTel ] = useState('');

    const onChange = ({ target: { value } } ) => {
        updateInputTel(value)
    }

    const sendRequest = async () => {
        try {
            const response = await fetch('https://developer.mozilla.org/ru/docs/Web/API/Fetch_API/Using_Fetch', {
                method: 'POST',
                body: JSON.stringify({ inputTel }),
                // headers: {
                //   'Content-Type': 'application/json'
                // }
              });
              const json = await response.json();
        } catch (error) {
            console.error('Ошибка:', error);
        }
    }
   
    return (
        <div className='home-request-wrapper'>
            <div className='container'>
                <div className='home-request'>
                    <p className='home-request__text'>
                        Оставьте  заявку и 
                        мы перезвоним вам!
                    </p>
                    <form className='home-request__from'>
                        <div className='home-request__from-group'>
                            <input 
                                type='text'
                                className='home-request__control'
                                onChange={onChange}
                                placeholder='Телефон / почта'
                                value={inputTel}/>
                            <button
                                type='button'
                                className='btn home-request__send-request'
                                onClick={sendRequest}>
                                Оставить заявку
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
};

export default HomeRequest;