import React from 'react';

import benefitsImg1 from '../../../asset/img/benefits/benefits_1.svg';
import benefitsImg2 from '../../../asset/img/benefits/benefits_2.svg';
import benefitsImg3 from '../../../asset/img/benefits/benefits_3.svg';
import benefitsImg4 from '../../../asset/img/benefits/benefits_4.svg';
import benefitsImg5 from '../../../asset/img/benefits/benefits_5.svg';
import benefitsImg6 from '../../../asset/img/benefits/benefits_6.svg';

import './HomeBenefits.scss';

const HomeBenefits = () => {  
    return (
        <div className='container js-stiky-name' data-stiky-number='02'  data-stiky-title='преимущества'>
            <div className='home-benefits'>
                <h4 className='title home-benefits__title'>
                    Преимущества
                </h4>
                <div className='section-name-wrap'>
                    <div className='home-benefits__grid home-benefits-grid'>
                        <div className='home-benefits-grid__item'>
                            <div className='home-benefits-grid__item-img-wrap'>
                                <img className='home-benefits-grid__item-img' src={benefitsImg1} alt='' />
                            </div>
                            <p className='home-benefits-grid__item-content'>
                                Высокая скорость 
                                выполнения 
                            </p>
                        </div>

                        <div className='home-benefits-grid__item'>
                            <div className='home-benefits-grid__item-img-wrap'>
                                <img className='home-benefits-grid__item-img' src={benefitsImg2} alt='' />
                            </div>
                            <p className='home-benefits-grid__item-content'>
                                Профессиональное проектирование
                            </p>
                        </div>

                        <div className='home-benefits-grid__item'>
                            <div className='home-benefits-grid__item-img-wrap'>
                                <img className='home-benefits-grid__item-img' src={benefitsImg3} alt='' />
                            </div>
                            <p className='home-benefits-grid__item-content'>
                                Наличие необходимых
                                допусков на 
                                осуществление работ
                            </p>
                        </div>

                        <div className='home-benefits-grid__item'>
                            <div className='home-benefits-grid__item-img-wrap'>
                                <img className='home-benefits-grid__item-img' src={benefitsImg4} alt='' />
                            </div>
                            <p className='home-benefits-grid__item-content'>
                                Широкая география 
                                деятельности
                            </p>
                        </div>

                        <div className='home-benefits-grid__item'>
                            <div className='home-benefits-grid__item-img-wrap'>
                                <img className='home-benefits-grid__item-img' src={benefitsImg5} alt='' />
                            </div>
                            <p className='home-benefits-grid__item-content'>
                                Бесплатная консультация
                            </p>
                        </div>

                        <div className='home-benefits-grid__item'>
                            <div className='home-benefits-grid__item-img-wrap'>
                                <img className='home-benefits-grid__item-img' src={benefitsImg6} alt='' />
                            </div>
                            <p className='home-benefits-grid__item-content'>
                                Инструментальный
                                контроль строительства
                            </p>
                        </div>
                    </div>

                    {/* <div className='section-name'>
                        <span className='section-name__number'> 02</span>
                        <span className='section-name__title'>преимущества</span>
                    </div> */}
                </div>
              
            </div>
        </div>
    )
};

export default HomeBenefits;