import React from 'react';

import './HomeAbout.scss';

const HomeAbout = () => {
   
    return (
        <div className='home-about-wrap'>
            <div className='container'>
                <div className='home-about js-stiky-name' data-stiky-number='04'  data-stiky-title='о нас'>
                    <h4 className='title home-about__title'>
                        О компании
                    </h4>
                    <div className=' section-name-wrap'>
                        <div className='home-about__content'>
                            <h6 className='home-about__sub-title'>
                                Строительная компания «Develop Group» 
                            </h6>
                            <p className='home-about__text'>
                                Предлагает как комплексные решения «под ключ», 
                                так и отдельные услуги по выполнению различных 
                                видов строительных работ на разных этапах 
                                строительства. С момента основания компания 
                                выступает субподрядной организацией 
                                строительных компаний, исполняющих строительство 
                                и реконструкцию строительных обьектов промышленного 
                                и гражданского строительства. Наша задача — 
                                заботиться о наших клиентах, делая их жизнь 
                                более комфортной и функциональной.
                            </p>
                        </div>
                        
                        {/* <div className='section-name'>
                            <span className='section-name__number'> 04</span>
                            <span className='section-name__title'>о нас</span>
                        </div> */}
                    </div>
                </div>
            </div>
        </div>
    )
};

export default HomeAbout;